import os
import datetime
import logging

class BaseConfig(object):
    SECRET_KEY = '2ee7eef9f0701ce3ec1454138b40695f74ca68a1b4611409'
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URI')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    JSON_AS_ASCII = False
    JWT_SECRET_KEY = '96c1fe78147c5f170630fe27428df48c495e18e3f6d72a3d'
    JWT_ACCESS_TOKEN_EXPIRES = datetime.timedelta(days=1)
    PAGARME_APIKEY = 'ak_test_Fdo1KyqBTdnTFeLgBhkgRcgm9hwdzd'
    SWAGGER = {
        "swagger_version": "2.0",
        "specs": [
            {
                "version": "0.0.1",
                "title": "Ecommerce Api",
                "endpoint": "/api",
                "route": "/api/spec",
                "basePath": "/",
                "url": "/apidocs/"
            }
        ]
    }
    LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    LOGGING_LOCATION = 'shopcart.log'
    LOGGING_LEVEL = logging.DEBUG

class DevelopmentConfig(BaseConfig):
    DEBUG = True

class ProductionConfig(BaseConfig):
    DEBUG = False
    LOGGING_LEVEL = logging.ERROR

class TestingConfig(BaseConfig):
    DEBUG = False
    TESTING = True

app_config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestingConfig,
    'default': DevelopmentConfig
}