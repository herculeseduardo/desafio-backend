# coding: utf-8
from os import path
import logging
from flask import Flask
from flasgger import Swagger
from flask_jwt_extended import JWTManager
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from .config import app_config

db = SQLAlchemy()
jwt = JWTManager()
swag = Swagger()
api = Api()

def create_app(config_name):
    instance_path = path.join(
        path.abspath(path.dirname(__file__))
    )
    app = Flask('ecommerce', instance_path=instance_path, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')
    
    # Configure logging
    handler = logging.FileHandler(app.config['LOGGING_LOCATION'])
    handler.setLevel(app.config['LOGGING_LEVEL'])
    formatter = logging.Formatter(app.config['LOGGING_FORMAT'])
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)

    app.app_context().push()

    api.init_app(app)
    db.init_app(app)
    jwt.init_app(app)
    swag.init_app(app)

    # Para simplificar a criação das tabelas será usada esta função.
    # Uma solução usando flask-migrate seria o ideal
    @app.before_first_request
    def create_database():
        db.create_all()

    from ecommerce.api.routes import route_api
    app.register_blueprint(route_api, url_prefix='/api')

    from ecommerce.payment import payment as payment_pagarme
    app.register_blueprint(payment_pagarme)

    return app