from flask import Blueprint, jsonify, abort, request
from flask_restful import Resource, Api, reqparse
from flask_jwt_extended import (
    jwt_required, create_access_token, get_jwt_identity
)
from flask_responses import json_response
from functools import wraps
from flask import current_app
from ecommerce.models import User, Product, Fator, Shopcart
from ecommerce.schemas import (
    product_schema, products_schema, fator_schema, fatores_schema,
    user_schema, users_schema)
from ..payment.pagarme import make_payment

route_api = Blueprint('api', __name__)
api = Api(route_api)

parser = reqparse.RequestParser()

# Criado decorator que verifica se o usuário autenticado
# possui o papel de ADMIN para algumas ações
def requires_admin(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        current_user = get_jwt_identity()
        user = User.query.get(current_user['user_id'])
        adm = [r.name for r in user.roles if r.name == 'ADMIN']
        if not adm:
            return {'message': 'user is not admin'}, 401
        return fn(*args, **kwargs)
    return wrapper

class UserListResource(Resource):
    @jwt_required
    @requires_admin
    def get(self):
        """
        User list
        ---
        tags:
          - User
        parameters:
          - in: header
            name: Authorization
            required: true
            type: string
        responses:
          200:
            description: Return user list
        """
        users = User.query.all()
        result = users_schema.dump(users)
        return jsonify({'users': result.data})

class UserResource(Resource):
    @jwt_required
    @requires_admin
    def get(self, user_id):
        """
        User by id
        ---
        tags:
          - User
        parameters:
          - in: path
            name: user_id
            required: true
            description: The ID of the user
            type: int
          - in: header
            name: Authorization
            required: true
            type: string            
        responses:
          200:
            description: The user
        """
        user = User.query.get(user_id)
        if user is None:
            return {'message': 'User not found'}, 404
        result = user_schema.dump(user)
        return jsonify({'user': result.data})

    @jwt_required
    @requires_admin
    def put(self, user_id):
        """
        Update user
        ---
        tags:
          - User
        parameters:
          - in: body
            name: body
            type: string
          - in: path
            name: user_id
            required: true
            description: The ID of the user
            type: string
          - in: header
            name: Authorization
            required: true
            type: string
        responses:
          204:
            description: The User has been updated
          500:
            description: Sorry, we were unable to process your request
        """
        data = request.get_json()
        if not data:
            return {'message': 'No input data provided'}, 400
        user = User.query.get(user_id)
        if user is None:
            return {'message': 'User not found'}, 404
        user.name = request.json.get('name', user.name)
        user.username = request.json.get('username', user.username)
        user.email = request.json.get('email', user.email)
        try:
            user.save_user()
            return {
                'message': 'User updated.'
            }, 204
        except Exception as error:
            current_app.logger.error('Server Error: %s', (error))
            return {'message': 'Sorry, we were unable to process your request'}, 500

    @jwt_required
    @requires_admin
    def delete(self, user_id):
        """
        This is an example
        ---
        tags:
          - User
        parameters:
          - in: path
            name: user_id
            required: true
            description: The ID of the user
            type: int
          - in: header
            name: Authorization
            required: true
            type: string
        responses:
          204:
            description: The User has been deleted
          500:
            description: Sorry, we were unable to process your request
        """
        user = User.query.get(user_id)
        if user is None:
            return {'message': 'User not found'}, 404
        try:
            user.delete_user()
            return {
                    'message': 'User deleted.'
                }, 204
        except Exception as error:
            current_app.logger.error('Server Error: %s', (error))
            return {'message': 'Sorry, we were unable to process your request'}, 500

class UserRegistration(Resource):
    def post(self):
        """
        User Registration
        ---
        tags:
          - User
        parameters:
          - in: body
            name: body
            type: string
        responses:
          201:
            description: The user has been created
          500:
            description: Sorry, we were unable to process your request
        """
        parser.add_argument('email', help='This field cannot be blank', required=True)
        data = parser.parse_args()
        if User.find_by_username(data['username']):
            return {'message': 'User {} already exists'. format(data['username'])}

        user = User(
            username = data['username'],
            password = User.generate_hash(data['password']),
            email = data['email'],
        )

        try:
            user.save_user()
            return {
                'message': 'User {} was created'.format( data['username'])
            }, 201
        except Exception as error:
            current_app.logger.error('Server Error: %s', (error))
            return {'message': 'Sorry, we were unable to process your request'}, 500

class Login(Resource):
    def post(self):
        """
        Login
        ---
        tags:
          - User
        parameters:
          - in: body
            name: body
            type: string
        responses:
          200:
            description: The user has been logged
          500:
            description: Sorry, we were unable to process your request
        """
        parser.add_argument('username', help='This field cannot be blank', required=True)
        parser.add_argument('password', help='This field cannot be blank', required=True)
        data = parser.parse_args()
        current_user = User.find_by_username(data['username'])

        if not current_user:
            return {'message': 'User {} doesn\'t exist'.format(data['username'])}
        
        if User.verify_hash(data['password'], current_user.password):
            roles = [r.name for r in current_user.roles]
            access_token = create_access_token(
                identity={'user_id': current_user.user_id, 'username': current_user.username, 'roles': roles})
            return {
                    'message': 'Logged in as {}'.format(current_user.username),
                    'access_token': access_token
                }, 200
        else:
            return {'message': 'Wrong credentials'}

class ProductListResource(Resource):
    def get(self):
        """
        Product List
        ---
        tags:
          - Product
        responses:
          200:
            description: The product list
        """
        products = Product.query.all()
        result = products_schema.dump(products)
        return jsonify({'products': result.data})
    
    @jwt_required
    @requires_admin
    def post(self):
        """
        Add new product
        ---
        tags:
          - Product
        parameters:
          - in: body
            name: body
            type: string
          - in: header
            name: Authorization
            required: true
            type: string
        responses:
          201:
            description: The Product has been created
          500:
            description: Sorry, we were unable to process your request
        """
        data = request.get_json()
        if not data:
            return {'message': 'No input data provided'}, 400

        product_name = data['name']
        product_description = data['description']
        product_image = data['image']
        product_price = data['price']
        fator_id = data['fator']
        fator = Fator.query.get(fator_id)
        if fator is None:
            return {'message': 'Fator not found'}, 404
        product = Product(name=product_name, description=product_description, image=product_image, price=product_price, fator=fator)
        try:
            product.save_product()
            return {
                'message': 'Created new product.'
            }, 201
        except Exception as error:
            current_app.logger.error('Server Error: %s', (error))
            return {'message': 'Sorry, we were unable to process your request'}, 500

class ProductResource(Resource):
    def get(self, product_id):
        """
        Product by id
        ---
        tags:
          - Product
        parameters:
          - in: path
            name: product_id
            required: true
            description: The ID of the product
            type: string
        responses:
          200:
            description: The Product by id
        """
        product = Product.query.get(product_id)
        if product is None:
            return {'message': 'Product not found'}, 404
        result = product_schema.dump(product)
        return jsonify({'product': result.data})

    @jwt_required
    @requires_admin
    def put(self, product_id):
        """
        Update product
        ---
        tags:
          - Product
        parameters:
          - in: body
            name: body
            type: string
          - in: path
            name: product_id
            required: true
            description: The ID of the product
            type: string
          - in: header
            name: Authorization
            required: true
            type: string
        responses:
          204:
            description: The Product has been updated
          500:
            description: Sorry, we were unable to process your request
        """
        data = request.get_json()
        if not data:
            return {'message': 'No input data provided'}, 400

        product = Product.query.get(product_id)
        if product is None:
            return {'message': 'Product not found'}, 404
        product.name = request.json.get('name', product.name)
        product.description = request.json.get('description', product.description)
        product.image = request.json.get('image', product.image)
        product.price = request.json.get('price', product.price)
        try:
            product.save_product()
            result = product_schema.dump(Product.query.get(product.product_id))
            return {
                'message': 'Updated Product.',
                "product": result.data
            }, 204
        except Exception as error:
            current_app.logger.error('Server Error: %s', (error))
            return {'message': 'Sorry, we were unable to process your request'}, 500

    @jwt_required
    @requires_admin
    def delete(self, product_id):
        """
        Remove product
        ---
        tags:
          - Product
        parameters:
          - in: header
            name: Authorization
            required: true
            type: string
          - in: path
            name: product_id
            required: true
            description: The ID of the product
            type: int
        responses:
          204:
            description: The Product has been deleted
          500:
            description: Sorry, we were unable to process your request
        """
        product = Product.query.get(product_id)
        if product is None:
            return {'message': 'Product not found'}, 404
        try:
            product.delete_product()
            return {
                    'message': 'Product deleted.'
                }, 204
        except Exception as error:
            current_app.logger.error('Server Error: %s', (error))
            return {'message': 'Sorry, we were unable to process your request'}, 500

class FatorListResource(Resource):
    def get(self):
        """
        Fator list
        ---
        tags:
          - Fator
        responses:
          200:
            description: Return fator list
        """
        fatores = Fator.query.all()
        result = fatores_schema.dump(fatores)
        return jsonify({'fatores': result.data})

    @jwt_required
    @requires_admin
    def post(self):
        """
        Add new fator
        ---
        tags:
          - Fator
        parameters:
          - in: body
            name: body
            type: string
          - in: header
            name: Authorization
            required: true
            type: string
        responses:
          201:
            description: The fator has been created
          500:
            description: Sorry, we were unable to process your request
        """
        data = request.get_json()
        if not data:
            return {'message': 'No input data provided'}, 400

        fator_name = data['name']
        fator_description = data['description']

        fator = Fator(name=fator_name, description=fator_description)
        try:
            fator.save_fator()
            return {
                'message': 'Created new Fator.'
            }, 201
        except Exception as error:
            current_app.logger.error('Server Error: %s', (error))
            return {'message': 'Sorry, we were unable to process your request'}, 500

class FatorResource(Resource):
    def get(self, fator_id):
        """
        Fator by id
        ---
        tags:
          - Fator
        parameters:
          - in: path
            name: fator_id
            required: true
            description: The ID of the fator
            type: string
        responses:
          200:
            description: The fator by id
        """
        fator = Fator.query.get(fator_id)
        if fator is None:
            return {'message': 'Fator not found'}, 404
        result = fator_schema.dump(fator)
        return jsonify({'fator': result.data})
    
    @jwt_required
    @requires_admin
    def put(self, fator_id):
        """
        Update fator
        ---
        tags:
          - Fator
        parameters:
          - in: body
            name: body
            type: string
          - in: path
            name: fator_id
            required: true
            description: The ID of the fator
            type: string
          - in: header
            name: Authorization
            required: true
            type: string
        responses:
          204:
            description: The fator has been updated
          500:
            description: Sorry, we were unable to process your request
        """
        data = request.get_json()
        if not data:
            return {'message': 'No input data provided'}, 400

        fator = Fator.query.get(fator_id)
        if fator is None:
            return {'message': 'Fator not found'}, 404

        fator.name = request.json.get('name', fator.name)
        fator.description = request.json.get('description', fator.description)

        try:
            fator.save_fator()
            result = fator_schema.dump(fator.query.get(fator.fator_id))
            return {
                'message': 'Created new Fator.',
                "fator": result.data
            }, 204
        except Exception as error:
            current_app.logger.error('Server Error: %s', (error))
            return {'message': 'Sorry, we were unable to process your request'}, 500
    
    @jwt_required
    @requires_admin
    def delete(self, fator_id):
        """
        Remove fator
        ---
        tags:
          - Fator
        parameters:
          - in: path
            name: fator_id
            required: true
          - in: header
            name: Authorization
            required: true
            type: string
        responses:
          201:
            description: The fator has been deleted
          500:
            description: Sorry, we were unable to process your request
        """
        fator = Fator.query.get(fator_id)
        if fator is None:
            return {'message': 'Fator not found'}, 404
        try:
            fator.delete_fator()
            return {
                    'message': 'Fator deleted.'
                }, 204
        except Exception as error:
            current_app.logger.error('Server Error: %s', (error))
            return {'message': 'Sorry, we were unable to process your request'}, 500

class ShopcartResource(Resource):
    @jwt_required
    def get(self):
        """
        List items cart
        ---
        tags:
          - Shopcart
        parameters:
          - in: header
            name: Authorization
            required: true
            type: string
        responses:
          201:
            description: The shopcart items
        """
        current_user = get_jwt_identity()
        cart_list = Shopcart.find_items_by_user(current_user['user_id'])
        if not cart_list:
            return json_response({'message': 'Empty cart'}, status_code=400)
        ret = Shopcart.process_shopcart(cart_list)
        return json_response({'cart':ret}, status_code=201)

    @jwt_required
    def post(self):
        """
        Add item cart
        ---
        tags:
          - Shopcart
        parameters:
          - in: header
            name: Authorization
            required: true
            type: string
          - in: body
            name: body
            type: string
        responses:
          201:
            description: Add item to cart
          500:
            description: Sorry, we were unable to process your request
        """
        parser.add_argument('product_id', help='This field cannot be blank', required=True)
        parser.add_argument('quantity', help='This field cannot be blank', required=True)
        data = parser.parse_args()
        item_quantity = data['quantity']
        current_user = get_jwt_identity()
        user = User.query.get(current_user['user_id'])
        if user is None:
            return {'message': 'User not found'}, 404
        product = Product.query.get(data['product_id'])
        if product is None:
            return {'message': 'Product not found'}, 404
        cart = Shopcart(user_id=user.user_id, product_id=product.product_id, quantity=item_quantity)
        try:
            cart.save_shopcart()
            return {
                'message': 'Item added.'
            }, 201
        except Exception as error:
            current_app.logger.error('Server Error: %s', (error))
            return {'message': 'Sorry, we were unable to process your request'}, 500

    @jwt_required
    def delete(self):
        """
        Remove Shopcart
        ---
        tags:
          - Shopcart
        parameters:
          - in: body
            name: body
            type: string
            required: true
          - in: header
            name: Authorization
            required: true
            type: string
        responses:
          201:
            description: The item has been deleted
          500:
            description: Sorry, we were unable to process your request
        """
        data = request.get_json()
        if not data:
            return {'message': 'No input data provided'}, 400

        current_user = get_jwt_identity()
        user = User.query.get(current_user['user_id'])
        if user is None:
            return {'message': 'User not found'}, 404
        product = Product.query.get(data['product_id'])
        if product is None:
            return {'message': 'Product not found'}, 404
        cart = Shopcart.find_item_by_product_id(product.product_id)
        if cart is None:
            return {'message': 'Cart not found'}, 404
        try:
            cart.remove_item()
            return {
                'message': 'Item deleted.'
            }, 204
        except Exception as error:
            current_app.logger.error('Server Error: %s', (error))
            return {'message': 'Sorry, we were unable to process your request'}, 500

class CheckoutResource(Resource):
    @jwt_required
    def post(self):
        """
        Checkout
        ---
        tags:
          - Checkout
        parameters:
          - in: header
            name: Authorization
            required: true
            type: string
        responses:
          200:
            description: Processing payment
          201:
            description: Payment done
          400:
            description: Empty cart
          500:
            description: Sorry, we were unable to process your request
        """
        current_user = get_jwt_identity()
        cart_list = Shopcart.find_items_by_user(current_user['user_id'])
        if not cart_list:
            return json_response({'message': 'Empty cart'}, status_code=400)
        try:
            cart_processed = Shopcart.process_shopcart(cart_list)
            response = make_payment(cart_processed)
            status = response['status']
            if status == 'paid':
                Shopcart.update_cart_checkout(current_user['user_id'])
                return {
                        'message': 'Payment done.'
                }, 201
            else:
                return {
                        'message': 'Processing payment.'
                }, 200
        except Exception as error:
            current_app.logger.error('Server Error: %s', (error))
            return {'message': 'Sorry, we were unable to process your request'}, 500


api.add_resource(UserRegistration, '/register')
api.add_resource(Login, '/login')
api.add_resource(UserListResource, '/users')
api.add_resource(UserResource, '/user/<user_id>')
api.add_resource(ProductListResource, '/products')
api.add_resource(ProductResource, '/product/<product_id>')
api.add_resource(FatorListResource, '/fator')
api.add_resource(FatorResource, '/fator/<fator_id>')
api.add_resource(ShopcartResource, '/cart')
api.add_resource(CheckoutResource, '/checkout')
