# Desafio Backend Developer
A Totvs está criando um aplicativo de e-commerce, foi feita uma planning com o Time o qual você é integrante e a Sprint começou. Suas tarefas são as seguintes: 

##### Criação de Produtos na loja ###
Criar os endpoints para adicionar e listar os produtos da loja. Esse produto deve ter os seguintes atributos: Nome, Descrição, Imagem, Valor e Fator. Existem três fatores A, B e C.  

##### Criação do Carrinho de Compras ###
Criar endpoints para adicionar, consultar e remover os produtos do carrinho de compras. A consulta dos produtos deve conter além da lista, a quantidade e valor total dos produtos adicionados seguindo a seguinte regra de fator

 - Os produtos de fator A tem um desconto progressivo de 1% a cada item adicionado no carrinho limitado a 5% de desconto. 
 - Os produtos de fator B tem um desconto progressivo de 5% a cada item adicionado no carrinho limitado a 15% de desconto.
 - Os produtos de fator C tem um desconto progressivo de 10% a cada item adicionado no carrinho limitado a 30% de desconto.
 - Um detalhe importante, o total de desconto do carrinho de compras não pode superar 30%.

##### Checkout pagar.me ###
Para finalizar, crie um endpoint para fazer o checkout do carrinho de compras através do pagar.me

#### Requisitos:
 - Utilizar Python ou Node JS.
 - Gestão de dependências via gerenciador de pacotes.
 - Persistir os dados.
 - Descreva no README os passos para execução do seu projeto.
 - Deixe seu repositório público para analise do Pull Request.

#### Ganha mais pontos:
 - Criação testes unitários
 - Garantia da segurança dos dados
 - Criação de uma estrutura de deploy da aplicação
 - Garantia a escalabilidade da aplicação (Pessoas | Infraestrutura)
 - Fique a vontade para adicionar mais features na loja desde que esteja dentro do contexto.

#### Submissão
 - Criar um fork desse projeto e entregar via Pull Request

#### Prazo de Entrega
 - 4 Dias

#### Dados de acesso a api do pagar.me
 - Documentação: https://docs.pagar.me/reference
 - Endpoint para o Checkout: https://api.pagar.me/1/transactions
 - ApiKey: ak_test_Fdo1KyqBTdnTFeLgBhkgRcgm9hwdzd
###Json de Envio:
```js
{
    "amount": 21000,
    "card_number": "4111111111111111",
    "card_cvv": "123",
    "card_expiration_date": "0922",
    "card_holder_name": "João das Neves",
    "customer": {
      "external_id": "#3311",
      "name": "João das Neves Braulio",
      "type": "individual",
      "country": "br",
      "email": "joaodasneves@got.com",
      "documents": [
        {
          "type": "cpf",
          "number": "00000000000"
        }
      ],
      "phone_numbers": ["+5511999998888", "+5511888889999"],
      "birthday": "1965-01-01"
    },
    "billing": {
      "name": "João das Neves",
      "address": {
        "country": "br",
        "state": "sp",
        "city": "Cotia",
        "neighborhood": "Rio Cotia",
        "street": "Rua Matrix",
        "street_number": "9999",
        "zipcode": "06714360"
      }
    },
    "shipping": {
      "name": "Neo Reeves",
      "fee": 1000,
      "delivery_date": "2000-12-21",
      "expedited": true,
      "address": {
        "country": "br",
        "state": "sp",
        "city": "Cotia",
        "neighborhood": "Rio Cotia",
        "street": "Rua Matrix",
        "street_number": "9999",
        "zipcode": "06714360"
      }
    },
    "items": [
   ITEMS_DO_SEU_CARRINHO
    ]
}
```
# Boa Sorte!



##Desenvolvimento aplicativo de e-commerce

### Getting Started

No desenvolvimento desse projeto procurei adicionar alguns itens que acredito que seriam interessantes para este desafio, como um ORM de banco de dados, documentação da API, JWT, logging. Muita coisa poderia ser evoluída e adicionada, mas acredito que consegui atender aos requisitos do desafio.
Para executar o projeto é necessário seguir o passos abaixo:

### Dependências

```
git
python3.5 ou superior
mysql
docker
virtualenv
pip

```

### Installing

Instalar os pré-requisitos de acordo com a plataforma

No ubuntu é necessário instalar os pacotes:

```
sudo apt-get install python3-dev libmysqlclient-dev
```

### Run

Para rodar o projeto é necessário que o banco de dados (mysql) esteja rodando e o database "shopcart_db" criado.

Se tiver o banco de dados rodando, apenas crie o database com o seguinte comando:

```
CREATE DATABASE shopcart_db CHARACTER SET utf8 COLLATE utf8_general_ci;
```

Depois importe a carga inicial do projeto através do script 'shopcart.sql'
Execute o comando:

```
mysql -u USUARIO -p shopcart_db < shopcart.sql
```

Clonar o projeto:

```
git clone git@bitbucket.org:hercs/app-shopcart.git
```

Após clonar o projeto criar uma variável de ambiente informando onde o banco de dados está rodando:

```
export DATABASE_URI='mysql://USURIO:SENHA@HOST:PORT/shopcart_db'
```

Em seguida, na raíz do projeto, crie um ambiente virtual com o comando:

```
virtualenv -p python3 venv
```

Ative o virtualenv

```
source venv/bin/activate
```

Instale as dependências

```
pip install -r requirements.txt
```

Rode o projeto com o comando:

```
python run.py
```

Acesse o endereço: http://localhost:5000/apidocs/

Se quiser rodar em uma porta diferente defina a variável de ambiente:
```
export PORT=8080
```
### Testando a aplicação

O banco de dados já está populado com alguns registros, sendo possível realizar alguns testes.

No endpoint de produtos (http://localhost:5000/apidocs/#!/Product) click em **GET** /api/products para ver a lista de produtos sem precisar de autenticação.

http://localhost:5000/apidocs/#!/Product/get_api_products

Para adicionar um item ao carrinho precisa estar autenticado no sistema, sendo necessário realizar login.
Click no endpoint de usuário: http://localhost:5000/apidocs/#!/User

```
http://localhost:5000/apidocs/#!/User/post_api_login
```
No corpo (body) da requisição coloque o json:

```
{
"username":"admin",
"password":"admin"
}
```
No response body será o retirnado o token de acesso, copie o token para realizar os testes que necessitam de autenticação.

```
"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0eXBlI..."
```

Adicione um item ao carrinho, clicando em http://localhost:5000/apidocs/#!/Shopcart

E depois click em http://localhost:5000/apidocs/#!/Shopcart/post_api_cart

No campo Authorization informe o token gerado no login da seguinte forma:

```
Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0eXBlI...
```

E no campo Body da requisição coloque o json a seguir e envie:

```
{
"product_id":"1",
"quantity":"1"
}
```

Verifique o carrinho em http://localhost:5000/apidocs/#!/Shopcart/get_api_cart  e informando o token

```
Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0eXBlI...
```

Adicione outro produto seguindo os mesmo passos:

E no campo Body da requisição coloque o json a seguir e envie:

```
{
"product_id":"2",
"quantity":"3"
}
```
Envie e verifique

Remova um item do carrinho clicando em http://localhost:5000/apidocs/#!/Shopcart/delete_api_cart  e informando o token

```
Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0eXBlI...
```
E no campo Body da requisição coloque o json a seguir e envie:

```
{
"product_id":"1"
}
```
Envie e verifique

Para finalizar realize o **checkout** do carrinho:

Click em http://localhost:5000/apidocs/#!/Checkout

E depois em http://localhost:5000/apidocs/#!/Checkout/post_api_checkout

Apenas informe o token e envie

```
Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0eXBlI...
```

Você receberá no response body:

```
{
  "message": "Payment done."
}
```

Agora executando o **GET** do carrinho você receberá uma mensagem de carrinho vazio.


## License

This project is licensed under the MIT License